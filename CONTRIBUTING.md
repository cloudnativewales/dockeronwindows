# Contributing to the Book Club

This document covers the process for contributing to the articles and code samples that are hosted on the [Docker on Windows Book Club](https://dockeronwindows.cloudnativewales.io/). Contributions may be as simple as typo corrections or as complex as new articles.

## Git

### How to make a simple correction or suggestion

Articles are stored in the repository as Markdown files. Simple changes to the content of a Markdown file can be made within the Web IDE.

Go to the repository ([link here](https://gitlab.com/cloudnativewales/dockeronwindows/)) and find the file with the typo.  Click *Edit* located on the top right hand corner of the window and make the changes to the file.

Once completed, update the *Commit Message* to be relevant to the changes that have been made, and create a new target branch, for example, *TypoInit* and click *Commit changes*.  Ensure that *Start a **new merge request** with these changes* is checked to create a merge request for the branch to potentially get merged.

### More to come

If you're new to Git, we'll look to help you get stated using the repository locally, and will update the contribution guide accordingly (or why not try making a pull request for updating the Contribution guide!)

## Hugo

1. Fork, clone or download this project
1. [Install Hugo](https://gohugo.io/getting-started/installing/)
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation](https://gohugo.io/overview/introduction/).

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.