---
title: About Docker on Windows
comments: false
---

![Docker on Windows](/img/dockeronwindows/dockeronwindows.png)

## Book Description

Docker is a platform for running server applications in lightweight units called containers. You can run Docker on Windows Server 2016 and Windows 10, and run your existing apps in containers to get significant improvements in efficiency, security, and portability.

This book teaches you all you need to know about Docker on Windows, from 101 to deploying highly-available workloads in production. This book takes you on a Docker journey, starting with the key concepts and simple examples of how to run .NET Framework and .NET Core apps in Windows Docker containers. Then it moves on to more complex examples—using Docker to modernize the architecture and development of traditional ASP.NET and SQL Server apps.

The examples show you how to break up monoliths into distributed apps and deploy them to a clustered environment in the cloud, using the exact same artefacts you use to run them locally. To help you move confidently to production, it then explains Docker security, and the management and support options.

The book finishes with guidance on getting started with Docker in your own projects, together with some real-world case studies for Docker implementations, from small-scale on-premises apps to very large-scale apps running on Azure.

[Docker On Windows](https://www.packtpub.com/virtualization-and-cloud/docker-windows)

## Authors

### Elton Stoneman

![Elton Stoneman](/img/dockeronwindows/eltonstoneman.jpeg)

Elton Stoneman has been a Microsoft MVP for 8 years and a Pluralsight author for 5 years, and now he works for Docker, Inc. Before joining Docker, he spent 15 years as a consultant, architecting and delivering very large and very successful solutions built on .NET and powered by Windows and Azure.

All the years he worked with Windows, Elton had a secret Linux server in his attic or garage, running core services for the home, such as music servers and file servers. When Docker started to take hold in the Linux world, Elton had early experience in a cross platform project he worked on, got hooked, and started to focus on containers. He was made a Docker Captain, and for a time, was one of only two people in the world who were both a Microsoft MVP and a Docker Captain.

Elton blogs about Docker, tweets about Docker, and speaks about Docker all the time. He is a regular at local events and user groups; you will often see him at Docker London, London DevOps, and WinOps London. He's also had great fun speaking at fantastic conferences around the world, including DockerCon, NDC London, SDD, DevSum, and NDC Oslo.