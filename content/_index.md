## What Is A CNW Book Club

Find out more about the Cloud Native Wales (CNW) Book Club [here](https://blog.cloudnativewales.io/bookclub).

## What Do I Need To Do

The Book Club will start on Friday June 15th 2018.  You'll be committed to reading the book over 8-12 weeks.  A private online community will be setup to discuss details of the book and on completion of the book, we'll have a Hangout with the Author, [Elton Stoneman](https://twitter.com/eltonstoneman).

## What Do I Get

You'll receive a free digital copy of the book, access to the discussion group and any swag that we can find.  Elton has also offered a 1 month trial to Pluralsight which will be provided at the start of the event.

## How Do I Sign Up

**Spaces are limited**, so please book your space [here](https://www.eventbrite.co.uk/e/cnw-book-club-docker-on-windows-by-elton-stoneman-tickets-46192256282).