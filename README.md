# Docker on Windows by Elton Stoneman

> A Cloud Native Wales (CNW) Book Club

## References

* [Book Club Announcement](https://blog.cloudnativewales.io/bookclub/)
* [Docker on Windows Announcement](https://blog.cloudnativewales.io/dockeronwindows/)
* [Sign Up](https://www.eventbrite.co.uk/e/cnw-book-club-docker-on-windows-by-elton-stoneman-tickets-46192256282)
* [Contribution Guide](https://gitlab.com/cloudnativewales/dockeronwindows/blob/master/CONTRIBUTING.md)